package com.example.bitsapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bitsapp.R
import com.example.bitsapp.data.model.Book
import com.example.bitsapp.databinding.ActivityDetaiBookBinding
import com.example.bitsapp.viewModel.DetailViewModel

class DetailBookActivity : AppCompatActivity() {

    companion object {
        const val BOOK = "book"
    }

    private lateinit var binding: ActivityDetaiBookBinding
    private lateinit var viewModel: DetailViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val book = intent.getSerializableExtra(BOOK)
        if (book != null && book is Book) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_detai_book)
            binding.lifecycleOwner = this
            binding.viewModel = ViewModelProvider(this, DetailViewModel.Factory(book)).get(DetailViewModel::class.java)
            setSupportActionBar(binding.toolbar)
        } else {
            finish()
        }
    }
}
