package com.example.bitsapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.bitsapp.R
import com.example.bitsapp.data.model.Book
import com.example.bitsapp.databinding.ItemBooksBinding

open class BooksAdapter(private val context: Context, private val books: List<Book>): RecyclerView.Adapter<BooksAdapter.Holder>() {

    private val layoutInflater = LayoutInflater.from(context)

    var onItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding: ItemBooksBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_books, parent, false)
        return Holder(binding)
    }

    override fun getItemCount(): Int {
       return books.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.setBook(books[position])
        onItemClickListener?.apply {
            holder.setClick(this)
        }
    }

    class Holder(private val binding: ItemBooksBinding): RecyclerView.ViewHolder(binding.root) {
        fun setBook(book: Book) {
            binding.item = book
        }

        fun setClick(onItemClickListener: OnItemClickListener) {
            binding.root.setOnClickListener {
                onItemClickListener.onItemClick(binding.root, position)
            }
        }

    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }

}