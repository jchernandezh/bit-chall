package com.example.bitsapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bitsapp.R
import com.example.bitsapp.data.model.Book
import com.example.bitsapp.data.model.RemoteError
import com.example.bitsapp.databinding.ActivityMainBinding
import com.example.bitsapp.ui.adapter.BooksAdapter
import com.example.bitsapp.utils.ResponseObserver
import com.example.bitsapp.viewModel.MainViewModel
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), BooksAdapter.OnItemClickListener {

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainViewModel
    private var adapter: BooksAdapter? = null

    private val handler = Handler()
    private val runnable = Runnable {
        binding.swipeLayout.isRefreshing = true
        viewModel.getAvailableBooks()
    }

    private val booksResponseObserver = object : ResponseObserver<List<Book>> {
        override fun onSuccess() {
           onBooksRespond()
        }

        override fun onError(error: RemoteError) {
            onBooksError(error)
        }
    }





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.getAvailableBooks()
        viewModel.responseBooks.observe(this, booksResponseObserver)
        binding.rvBooks.layoutManager = LinearLayoutManager(this)
        binding.swipeLayout.setOnRefreshListener {
            viewModel.getAvailableBooks()
        }
        binding.viewModel = viewModel
    }

    private fun onBooksRespond() {
        adapter = BooksAdapter(this, viewModel.availableBooks!!)
        adapter!!.onItemClickListener = this
        binding.rvBooks.adapter = adapter
        binding.swipeLayout.isRefreshing = false
        handler.postDelayed(runnable, TimeUnit.SECONDS.toMillis(30))
    }

    private fun onBooksError(error: RemoteError) {
        binding.swipeLayout.isRefreshing = false
        val builder = AlertDialog.Builder(this)
        builder.setMessage("${error.error} (${error.statusCode})")
        builder.setPositiveButton(R.string.retry) { _, _ ->
            viewModel.getAvailableBooks()
        }
        builder.show()
    }

    override fun onItemClick(view: View, position: Int) {
        val detailIntent = Intent(this, DetailBookActivity::class.java)
        detailIntent.putExtra(DetailBookActivity.BOOK, viewModel.availableBooks!![position])
        startActivity(detailIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(runnable)
    }
}
