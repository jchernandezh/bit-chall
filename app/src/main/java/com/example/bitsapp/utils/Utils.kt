package com.example.bitsapp.utils

import android.annotation.TargetApi
import android.content.res.Configuration
import android.os.Build
import com.example.bitsapp.App
import java.text.NumberFormat
import java.util.*


object Utils {

    fun parseAmount(value: Double):String {
        return NumberFormat.getCurrencyInstance(getSystemLocale(App.INSTANCE!!.applicationContext.resources.configuration)).format(value)

    }

    private fun getSystemLocale(config: Configuration): Locale {
       return if (Build.VERSION.SDK_INT ==  Build.VERSION_CODES.N) {
           config.locales.get(0)
       } else {
           config.locale
       }
    }
}