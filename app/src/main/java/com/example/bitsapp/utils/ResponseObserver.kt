package com.example.bitsapp.utils

import androidx.lifecycle.Observer
import com.example.bitsapp.data.model.RemoteError
import com.example.bitsapp.data.model.ResponseObject

interface ResponseObserver<T> : Observer<ResponseObject<T>> {

    override fun onChanged(t: ResponseObject<T>) {
        if (t.status == ResponseObject.STATUS.SUCCESS) {
            onSuccess()
        } else if (t.status == ResponseObject.STATUS.ERROR){
            onError(t.error!!)
        }
    }

    fun onSuccess()
    fun onError(error: RemoteError)
}