package com.example.bitsapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bitsapp.data.model.Book
import com.example.bitsapp.data.model.ResponseObject
import com.example.bitsapp.data.remote.RemoteManager
import kotlinx.coroutines.launch

open class MainViewModel: ViewModel() {

    private val responseBooksData = MutableLiveData<ResponseObject<List<Book>>>()
    val responseBooks: LiveData<ResponseObject<List<Book>>>
    get() {
       return responseBooksData
    }
    private val remoteManager = RemoteManager()
    val availableBooks: List<Book>?
    get() {
        return responseBooks.value?.data
    }

    init {
        responseBooksData.value = ResponseObject()
    }

    fun getAvailableBooks() {
        viewModelScope.launch {
            val responseObject = remoteManager.getAvailableBooks()
            if (responseObject.status == ResponseObject.STATUS.SUCCESS) {
                responseObject.data!!.forEach {
                    val tickerResponse = remoteManager.getTicker(it.book)
                    if (tickerResponse.status == ResponseObject.STATUS.SUCCESS) {
                        it.ticker = tickerResponse.data!!
                    }
                }
            }
            responseBooksData.value = responseObject
        }
    }


}