package com.example.bitsapp.viewModel

import androidx.lifecycle.*
import com.example.bitsapp.data.model.Book
import com.example.bitsapp.data.remote.RemoteManager
import kotlinx.coroutines.launch
import com.example.bitsapp.R

open class DetailViewModel(bookModel: Book): ViewModel() {


    private val bookData = MutableLiveData<Book>()
    val book: LiveData<Book>
    get() {
        return bookData
    }
    private val remoteManager = RemoteManager()
    init {
        bookData.value = bookModel
    }

    fun getTicker() {
        viewModelScope.launch {
            remoteManager.getTicker(book.value!!.book)
        }
    }


    class Factory(private val book: Book): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getConstructor(Book::class.java)
                .newInstance(book)
        }
    }
}