package com.example.bitsapp.data.remote

import android.util.Log
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.example.bitsapp.data.model.Book
import com.example.bitsapp.data.model.RemoteError
import com.example.bitsapp.data.model.ResponseObject
import com.example.bitsapp.data.model.Ticker
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

open class RemoteManager: BaseRemoteManager() {

    suspend fun getAvailableBooks() = suspendCoroutine<ResponseObject<List<Book>>>{

        val request = JsonObjectRequest("$baseUrl/available_books", null,
            Response.Listener<JSONObject> {jsonResponse ->
                Log.i("response", jsonResponse.toString())
                if (jsonResponse.getBoolean("success")) {
                    val books =  GSON.fromJson<List<Book>>(jsonResponse.getJSONArray("payload").toString()
                        , object : TypeToken<List<Book>>() {}.type)
                    it.resume(ResponseObject(books))
                }
            }, Response.ErrorListener {error: VolleyError ->
                val status = error.networkResponse?.statusCode ?: 0
                val message = error.message ?: "Service error"
                it.resume(ResponseObject(RemoteError(status, message)))
        })

        addRequest(request)
    }

    suspend fun getTicker(book: String) = suspendCoroutine<ResponseObject<Ticker>> {
        val request = JsonObjectRequest("$baseUrl/ticker?book=$book", null,
        Response.Listener<JSONObject> {response ->

            if (response.getBoolean("success")) {
                val ticker = GSON.fromJson(response.getJSONObject("payload").toString(), Ticker::class.java)
                it.resume(ResponseObject(ticker))
            }
        }, Response.ErrorListener {error ->
                val status = error.networkResponse?.statusCode ?: 0
                val message = error.message ?: "Service error"
                it.resume(ResponseObject(RemoteError(status, message)))
            })

        addRequest(request)
    }

}