package com.example.bitsapp.data.remote

import com.android.volley.Request
import com.example.bitsapp.App
import com.example.bitsapp.R
import com.example.bitsapp.RequestSingleton
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder

open class BaseRemoteManager {

    private val requestSingleton: RequestSingleton
    protected val baseUrl: String
    protected final val GSON = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

    init {
        val context = App.INSTANCE!!.applicationContext
        requestSingleton = RequestSingleton(context)
        baseUrl = context.getString(R.string.url)
    }


    fun <T> addRequest(request: Request<T>) {
        requestSingleton.addToRequestQueue(request)
    }
}