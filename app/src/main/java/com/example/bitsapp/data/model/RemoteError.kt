package com.example.bitsapp.data.model

data class RemoteError(val statusCode: Int, val error: String)