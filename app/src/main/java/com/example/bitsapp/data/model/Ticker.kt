package com.example.bitsapp.data.model

import java.io.Serializable

data class Ticker(val volume: Double,
                  val high: Double,
                  val last: Double,
                  val low: Double,
                  val vwap: Double,
                  val ask: Double,
                  val bid: Double,
                  val createdAt: String): Serializable
