package com.example.bitsapp.data.model

import java.io.Serializable

data class Book(val book: String,
                val minimumAmount: Double,
                val maximumAmount: Double,
                val minimumPrice: Double,
                val maximumPrice: Double,
                val minimumValue: Double,
                val maximumValue: Double,
                var ticker: Ticker?): Serializable