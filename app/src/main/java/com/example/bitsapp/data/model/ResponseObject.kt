package com.example.bitsapp.data.model

data class ResponseObject<T>(val status: STATUS, val data: T?, val error: RemoteError?) {

    constructor(): this(STATUS.LOADING, null, null)
    constructor(data: T): this(STATUS.SUCCESS, data, null)
    constructor(error: RemoteError): this(STATUS.ERROR, null, error)

    enum class STATUS {
        SUCCESS, ERROR, LOADING
    }
}