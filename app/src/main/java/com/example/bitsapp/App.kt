package com.example.bitsapp

import android.app.Application

open class App: Application() {


    companion object {
        var INSTANCE: App? = null
    }
    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

    }

}